
class Transformer {

    constructor( { transformers = [], marker = "_pjson" } ) {

        this.transformers = transformers;
        this.marker       = marker;
        this.tagLookup    = {};

        for( var [ ix, transformer ] of Object.entries( this.transformers ) )
            this.tagLookup[ transformer.tag ] = ix;

    }

    encode( data ) {

        for( var transformer of this.transformers ) {

            if( transformer.test( data ) )
                return { value : transformer.encode( data ), [ this.marker ] : transformer.tag };

        }

        if( data instanceof Array )
            return data.map( x => this.encode( x ) );

        if( data instanceof Object ) {

            var results = {};
            for( var key of Object.keys( data ) )
                results[ key ] = this.encode( data[ key ] );
            return results;

        }

        return data;
            
    }

    decode( data ) {

        if( data instanceof Object && data[ this.marker ] ) 
            return this.transformers[ this.tagLookup[ data[ this.marker ] ] ].decode( data.value );

        if( data instanceof Array )
            return data.map( x => this.decode( x ) );

        if( data instanceof Object ) {

            var results = {};
            for( var key of Object.keys( data ) )
                results[ key ] = this.decode( data[ key ] );
            return results;

        }

        return data;

    }

}

module.exports = Transformer;
